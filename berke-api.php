<?php 

include_once('functions.php');
include_once('api.php');

if (!class_exists('BerkeApi')) {
    class BerkeApi {
        private $berkeCandidateIdField;
        private $wpdb;
        private $berkeNoJobId;

        public function __construct() {
            global $wpdb;
            $this->wpdb = &$wpdb;

            debugLog('looking up berkeCandidateId field');
            $query = "SELECT * FROM {$wpdb->prefix}bp_xprofile_fields where name = 'berkeCandidateId'";
            $results = $this->wpdb->get_results($query, OBJECT);
            $this->berkeCandidateIdField = $results[0]->id;

            $options = get_option('api-settings');

            $this->berkeNoJobId = $options['berkeNoJobId'];
        }

        public function getBerkeNoJobId() {
            return $this->berkeNoJobId;
        }

        public function getUserBerkeCandidateId() {
            $current_user_id = get_current_user_id();
            if ($current_user_id == 0) {
                throw new Exception('user must be logged in');
            }

            $query = "SELECT value FROM {$this->wpdb->prefix}bp_xprofile_data where user_id = '$current_user_id' and field_id = '$this->berkeCandidateIdField'";
            $results = $this->wpdb->get_results($query, OBJECT);
            if (count($results) > 0) {
                return $results[0]->value;
            }
            return null;
        }

        public function handleResponse($reqAction, $response) {
            $responseObject = json_decode($response);
            //debugLog('Handling response of ' . $response . ' for ' . $reqAction);
            if ($reqAction == 'CreateAssessment') {
                $candidateId = $responseObject->assessment->sourceCandidateId;
                $this->updateCandidateId($candidateId);
            }

        }

        private function updateCandidateId($candidateId) {
            $current_user_id = get_current_user_id();
            $query;
            if ($this->getUserBerkeCandidateId() == null ) {
                debugLog('Setting Berke Candidate Id');
                $query = "INSERT INTO {$this->wpdb->prefix}bp_xprofile_data (user_id, field_id, value, last_updated) VALUES ($current_user_id, $this->berkeCandidateIdField, '$candidateId', now())";
            }
            else {
                debugLog('Updating Berke Candidate Id');
                $query = "UPDATE {$this->wpdb->prefix}bp_xprofile_data SET value = '$candidateId', last_updated=now() WHERE user_id='$current_user_id' AND field_id=$this->berkeCandidateIdField";
            }

            $this->wpdb->query($query);
        }

        public function getCurrentUserType() {
            debugLog('Checking current user type');
            $current_user_id = get_current_user_id();
            $query = "SELECT data.value FROM {$this->wpdb->prefix}bp_xprofile_fields field JOIN {$this->wpdb->prefix}bp_xprofile_data data ON data.field_id = field.id WHERE field.name = 'User Type' AND field.type = 'selectbox' AND data.user_id = $current_user_id";
            
            $results = $this->wpdb->get_results($query, OBJECT);
            if (count($results) > 0) {
                $finalResult = $results[0]->value;
                debugLog("User is a $finalResult");
                return $finalResult;
            }

            debugLog('Could not determine user type');
            return null;
        }

        private function getGenericBerkeRequest($action) {
            $candidateId = $this->getUserBerkeCandidateId();

            $requestJson = '{"action":"' . $action . '", "data":{"sourceCandidateId":"' . $candidateId . '"}}';

            global $ApiHandler;
            $json_results = $ApiHandler->handleRequestWithData($requestJson, 'POST');

            debugLog("$action: $json_results");
            return json_decode($json_results);
        }

        public function getAssessmentStatus() {
            return $this->getGenericBerkeRequest('GetAssessmentStatus');
        }

        public function getMatches() {
            $query = "SELECT post_name AS postName, meta_value AS berkeId FROM {$this->wpdb->prefix}postmeta AS meta JOIN {$this->wpdb->prefix}posts AS pages ON meta.post_id = pages.ID WHERE meta.meta_key = '_career_berke_id'";
            $sqlResults = $this->wpdb->get_results($query, OBJECT);

            $current_user_id = get_current_user_id();
            //see what results we already have stored in the DB
            $currentMatchesQuery = "SELECT * FROM {$this->wpdb->prefix}berke_profile_results where user_id = $current_user_id";
            $currentMatchesResults = $this->wpdb->get_results($currentMatchesQuery, OBJECT);

            $apiResults = $this->getGenericBerkeRequest('GetCompletedAssessment');
            foreach($apiResults->assessment->jobFit as &$jobFit) {
                $jobFit->vocatioPageId = '';
                foreach($sqlResults as $sqlRow) {
                    if ($sqlRow->berkeId == $jobFit->sourceJobId) {
                        $jobFit->vocatioPageId = $sqlRow->postName;
                    }
                }

                $foundRow = false;
                foreach($currentMatchesResults as $sqlRow) {
                    if ($sqlRow->job_id == $jobFit->sourceJobId) {
                        debugLog('Job match record found');
                        $foundRow = true;
                        //check to see if it's correct
                        if ($sqlRow->score_numeric != $jobFit->fitPct || $sqlRow->score_text != $jobFit->fit || $sqlRow->report_url != $jobFit->reportUrl) {
                            $updateJobResultQuery = "UPDATE {$this->wpdb->prefix}berke_profile_results SET score_numeric=$jobFit->fitPct, score_text='$jobFit->fit', report_url='$jobFit->reportUrl' WHERE id=$sqlRow->id";
                            debugLog('Record changed, updating');
                            debugLog($updateJobResultQuery);
                            $this->wpdb->query($updateJobResultQuery);
                        }
                    }
                }
                if (!$foundRow) {
                    //do insert
                    debugLog('Inserting new job match result');
                    $insertJobResultQuery = "INSERT INTO {$this->wpdb->prefix}berke_profile_results (job_id, user_id, score_numeric, score_text, report_url) VALUES ('$jobFit->sourceJobId', $current_user_id, $jobFit->fitPct, '$jobFit->fit', '$jobFit->reportUrl')";
                    debugLog($insertJobResultQuery);
                    $this->wpdb->query($insertJobResultQuery);
                }

            }

            return $apiResults;
        }

        public function getPersonalizedReportUrl() {
            $reportUrl = $this->getJobReportUrl($this->berkeNoJobId);
            if ($reportUrl == null) {
                debugLog("Could not find personal report url");
            }

            return $reportUrl;
        }

        public function getJobReportUrl($jobId) {
            $current_user_id = get_current_user_id();
            $query = "SELECT report_url FROM {$this->wpdb->prefix}berke_profile_results WHERE user_id=$current_user_id AND job_id=$jobId";

            $results = $this->wpdb->get_results($query, OBJECT);
            if (count($results) > 0) {
                $finalResult = $results[0]->report_url;
                return $finalResult;
            }

            debugLog("Could not find report $jobId url");
            return null;
        }

    }
}

global $BerkeApi;
$BerkeApi = new BerkeApi();
