<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package Wordpress
 * @subpackage Kleo
 * @since Kleo 1.0
 */

require_once('berke-api.php');

function add_api_scripts() {
    wp_enqueue_script('client_side_api_scripts', plugins_url('api.js', __FILE__), array('jquery'));
}

add_action('wp_enqueue_scripts', 'add_api_scripts');

get_header();

get_template_part('page-parts/general-title-section');

get_template_part('page-parts/general-before-wrap');

$options = get_option('api-settings'); 
$apiEndpointUrl = site_url() . '/' . $options['apiWordpressPage'];

?>

<script type="text/javascript">
    pageOptions = { apiUrl: '<?php echo $apiEndpointUrl ?>' };
</script>

<?php 
    $current_user = wp_get_current_user(); 
    debugLog('Current user: ' . json_encode($current_user));
    if ($current_user->ID == 0) {
        echo 'not logged in';
    }
    
    if ($BerkeApi->getCurrentUserType() != 'Student') {
        echo 'You must be a student';
    }

    else {
        if ($BerkeApi->getUserBerkeCandidateId()) {
            $assessmentStatus = $BerkeApi->getAssessmentStatus();

            if ($assessmentStatus->assessmentStatus->status == "InProgress") {
?>
<h2>You are <?php echo $assessmentStatus->assessmentStatus->percentComplete; ?>% done with the assessment</h2>
<a href="<?php echo $assessmentStatus->assessmentStatus->assessmentUrl; ?>">Continue Assessment</a>

<?php
            }
            else if ($assessmentStatus->assessmentStatus->status == "Completed") {
                $matchData = $BerkeApi->getMatches();
?>
                <div class="pdfIframe">
                    <iframe src="<?php echo $BerkeApi->getPersonalizedReportUrl()?>"></iframe>
                </div>
                <a href="<?php echo $BerkeApi->getPersonalizedReportUrl()?>">Your Personalized Report</a><br><br>
<?php
                foreach($matchData->assessment->jobFit as $jobFit) {
                    if ($jobFit->sourceJobId != $BerkeApi->getBerkeNoJobId()) {
                        echo $jobFit->job . ' (' . $jobFit->fitPct . ') ';
                        if ($jobFit->vocatioPageId != '') {
                            echo '<a href="' . get_site_url() . '/careers/' . $jobFit->vocatioPageId . '/">Go to the career page</a>';
                        }
                        echo ' <a href="' . $BerkeApi->getJobReportUrl($jobFit->sourceJobId) . '">Job Report</a>';
                        echo '<br>';
                    }
                }
?>
<?php
            }
         }
        else {
    ?>
    First Name: <input type="text" id="firstName" value="<?php echo $current_user->first_name ?>"></input><br>
    Last Name: <input type="text" id="lastName" value="<?php echo $current_user->last_name ?>"></input><br>
    Click to set up an assessment <button id="submitAssessmentButton" onclick="createAssessment()">Submit</button>
    <br><br>
    Assessment URL: <a href="" id="assessmentUrl" target="_blank"></a>
        
<?php }} get_template_part('page-parts/general-after-wrap'); ?>

<?php get_footer(); ?>
