function createAssessment() {
    jQuery('#submitAssessmentButton').attr('disabled', 'disabled');

    var firstName = jQuery('#firstName').val();
    var lastName = jQuery('#lastName').val();

    var assessmentUrlAnchor = jQuery('#assessmentUrl');

    generateAssessmentLink(firstName, lastName, function(resp) {
        var url = resp.assessment.assessmentUrl;
        assessmentUrlAnchor.attr('href', url);
        assessmentUrlAnchor.html(url);
    });
}

function generateAssessmentLink(firstName, lastName, cb) {
    var postData = {action:'CreateAssessment',data:{firstName:firstName,lastName:lastName}};
    var postUrl = pageOptions.apiUrl;

    var ajaxOptions = {type:'POST',url:postUrl,success: cb, data:JSON.stringify(postData), contentType: 'application/json'};
    jQuery.ajax(ajaxOptions);
}
