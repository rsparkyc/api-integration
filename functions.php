<?php

date_default_timezone_set('UTC');

if (!function_exists('http_response_code'))
{
    function http_response_code($newcode = NULL) {
        static $code = 200;
        if($newcode !== NULL) {
            header('X-PHP-Response-Code: '.$newcode, true, $newcode);
            if(!headers_sent()) {
                $code = $newcode;
            }
        }       
        return $code;
    }
}

function error($err) {
    http_response_code(500);
    $errJson = '{"error": "' . $err . '"}';
    debugLog($errJson);
    return $errJson;
}

function debugLog($msg) {
    $date = new DateTime();
    file_put_contents('php://stderr', print_r('[' . $date->format('D M d H:i:s Y') . '] [PHP DEBUG] ' . $msg . PHP_EOL, TRUE));
}

?>
