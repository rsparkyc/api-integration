<?php

include_once 'functions.php';
include_once 'berke-api.php';

if (!class_exists('ApiHandler')) {
    class ApiHandler {

        function cURLcheckBasicFunctions()
        {
            return function_exists('curl_init')
                && function_exists('curl_setopt')
                && function_exists('curl_exec')
                && function_exists('curl_close');
        }

        function handleRequest() {
            $RequestMethod = $_SERVER['REQUEST_METHOD'];
            $postData = file_get_contents('php://input');
            header('Content-Type: application/json');

            return $this->handleRequestWithData($postData, $RequestMethod);
        }


        function handleRequestWithData($postData, $RequestMethod)
        {
            $options = get_option('api-settings');
            $apiEndpoint = $options['apiEndpoint'];

            if (!$this->cURLcheckBasicFunctions()) {
                return error('UNAVAILABLE: cURL Basic Functions');
            }
            $ch = curl_init();
            if ($ch) {

                $action;

                switch ($RequestMethod) {
                    case 'POST':
                        $postArray = json_decode($postData, true);
                        $action = $postArray['action'];

                        $modifiedPostData = $this->prepPostData($options, $postArray);

                        debugLog('data: ' . $modifiedPostData);

                        $url = $this->calculateUrl($apiEndpoint, $postArray);

                        if (!curl_setopt($ch, CURLOPT_URL, $url)) {
                            curl_close($ch); // to match curl_init()
                            return error('FAIL: curl_setopt(CURLOPT_URL)');
                        }
                        if (!curl_setopt($ch, CURLOPT_HEADER, 0)) {
                            return error('FAIL: curl_setopt(CURLOPT_HEADER)');
                        }

                        //Don't output result to the screen, instead we'll capture it
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                        curl_setopt($ch, CURLOPT_POST, true);

                        curl_setopt($ch, CURLOPT_POSTFIELDS, $modifiedPostData);

                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/x-www-form-urlencoded',
                            'Content-Length: ' . strlen($modifiedPostData),
                            'Accept: application/json')
                        );

                        debugLog('making ' . $RequestMethod . ' to ' . $url);
                        debugLog('   data: ' . $modifiedPostData);

                        break;
                    default:
                        return error('Unsupported Request Method (' . $RequestMethod . ')');
                }

                $output = curl_exec($ch);
                if (!$output) {
                    return error('FAIL: curl_exec()');
                }

                curl_close($ch);

                global $BerkeApi;

                $BerkeApi->handleResponse($action, $output);

                return $output;
            } else {
                return error('FAIL: curl_init()');
            }
        }

        function prepPostData($options, $postArray) {
            $newData = $postArray['data'];

            $additionalData = $options['apiDataModifier'];
            $additionalJson = json_decode($additionalData);
            foreach($additionalJson as $key=>$value) {
                $newData[$key] = $value;
            }

            $formEncoded = $this->convertToFormEncoded($newData);

            return $formEncoded;
        }

        function convertToFormEncoded($data) {
            $formStr = '';
            foreach($data as $key=>$value) {
                $formStr .= $key . '=' . $value . '&';
            }

            return rtrim($formStr, '&');
        }

        function calculateUrl($apiEndpoint, $postData) {
            return $apiEndpoint . $postData['action'];
        }
    }
}

global $ApiHandler;
$ApiHandler = new ApiHandler();

?>
