<?php
/**
* Plugin Name: API Integration
* Description: Plugin to integrate with a third party api
* Version: 0.0.1
* Author: Ryan Caskey
*/
if (!class_exists('ApiIntegrationPlugin')) {
    class ApiIntegrationPlugin {
        function __construct() {
            require_once(plugin_dir_path(__FILE__) . 'functions.php');

            $this->addActions();
            $this->enqueueScripts();

            $plugin = plugin_basename(__FILE__);
            add_filter('plugin_action_links_' . $plugin, array(&$this, 'apiIntegration_settings_link'));
            add_filter('page_attributes_dropdown_pages_args', array(&$this, 'register_api_template'));
            add_filter('wp_insert_post_data', array(&$this, 'register_api_template'));
            add_filter('template_include', array(&$this, 'view_api_template'));

            register_activation_hook(__FILE__, array(&$this, 'api_install'));

            $this->templates = array('api-page-template.php'=>'API Page Template',
                                     'signup-page-template.php'=>'Signup Page Template');
        }

        function api_install() {
            global $wpdb;

            $table_name = $wpdb->prefix . 'berke_profile_results';

            $sql = "CREATE TABLE wp_berke_profile_results (
                        id mediumint(9) NOT NULL AUTO_INCREMENT,
                        job_id varchar(255) NOT NULL,
                        user_id bigint(20) UNSIGNED NOT NULL,
                        score_numeric decimal(3,2) NOT NULL,
                        score_text varchar(255) NOT NULL,
                        report_url varchar(255) NOT NULL,
                        UNIQUE KEY (id),
                        FOREIGN KEY (user_id) REFERENCES wp_users(ID)
                    )";

             require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
             dbDelta($sql);
        }

        function register_api_template($atts) {
            // Create the key used for the themes cache
            $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

            // Retrieve the cache list. 
            // If it doesn't exist, or it's empty prepare an array
            $templates = wp_get_theme()->get_page_templates();
            if ( empty( $templates ) ) {
                    $templates = array();
            } 

            // New cache, therefore remove the old one
            wp_cache_delete( $cache_key , 'themes');

            // Now add our template to the list of templates by merging our templates
            // with the existing templates array from the cache.
            $templates = array_merge( $templates, $this->templates );

            // Add the modified cache to allow WordPress to pick it up for listing
            // available templates
            wp_cache_add( $cache_key, $templates, 'themes', 1800 );

            return $atts;
        }

        function view_api_template($template) {
            global $post;

            if (!isset($this->templates[get_post_meta( 
                $post->ID, '_wp_page_template', true)] ) ) {
                    return $template;
            } 

            $file = plugin_dir_path(__FILE__). get_post_meta( 
                $post->ID, '_wp_page_template', true 
            );

            // Just to be safe, we check if the file exist first
            if( file_exists( $file ) ) {
                return $file;
            } 
            else { echo $file; }

            return $template;
        }

        function enqueueScripts() {
            //we'll use this if we need to add any js files later
        }

        function addActions() {
            if(is_admin()) {
                add_action('admin_menu', array(&$this, 'create_apiIntegration_admin_menu'));
                add_action('wp_ajax_nopriv_handleRequest', array(&$this, 'handleRequest'));
                add_action('admin_init', array(&$this, 'register_settings'));
            }
        }
        
        function apiIntegrationInstall() {
            //probably nothing to do here, but we'll leave it just in case
        }

        function register_settings() {
            register_setting(
                'api-settings',
                'api-settings',
                array(&$this, 'sanitize_input')
            );
        }

        function sanitize_input($input) {
            $urlRegexPattern = '#(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))#iS';


            $apiEndpoint = $input['apiEndpoint'];
            $apiWordpressPage = $input['apiWordpressPage'];
            $apiDataModifier = $input['apiDataModifier'];
            $berkeNoJobId = $input['berkeNoJobId'];
            debugLog('sanitizing apiEndpoint (' . $apiEndpoint . ')');

            $match = preg_match($urlRegexPattern, $apiEndpoint);
            debugLog('Match: ' . $match);
            if (!$match) {
                add_settings_error('apiEndpoint', 'apiEndpoint', 'API URL Endpoint is not a valid URL');
            }

            debugLog('Ensuring apiDataModifier is JSON');
            $jsonBlob = json_decode($apiDataModifier);
            if ($jsonBlob == null) {
                add_settings_error('apiDataModifier', 'apiDataModifier', 'JSON is not valid');
            }


            debugLog('sanitizing apiWordpressPage (' . $apiWordpressPage . ')');
            debugLog('sanitizing apiDataModifier (' . $apiDataModifier . ')');
            return $input;
        }

        function create_apiIntegration_admin_menu() {
            add_options_page('API Integration', 'API Integration', 'manage_options', 'apiIntegration_settings', array($this, 'create_apiIntegration_settings_page'));
        }

        function create_apiIntegration_settings_page() {
            debugLog('Showing apiIntegration Settings page');
            ?>
            <div class="wrap">
                <h2>Manage API settings</h2>
                <form id="apiSettingsForm" action="options.php" method="post">
                <?php 
                    settings_fields('api-settings');
                    do_settings_sections('api-settings'); 
                    $options = get_option('api-settings');
                    ?>

                    <table class="form-table">
                        <tr valign="top" id="apiEndpointRow">
                            <th scope="row"><label for="apiEndpoint">API URL Endpoint:</label></th>
                            <td><input name="api-settings[apiEndpoint]" type="text" placeholder="API URL Endpoint" class="medium-text" value="<?php echo $options['apiEndpoint']; ?>"/></td>
                        </tr>
                        <tr valign="top" id="apiWordpressPageRow">
                            <th scope="row"><label for="apiWordpressPage">API Wordpress Page:</label></th>
                            <td><?php echo site_url() . '/'; ?><input name="api-settings[apiWordpressPage]" type="text" placeholder="The wordpress page configured to broker API requets" class="medium-text" value="<?php echo $options['apiWordpressPage']; ?>"/></td>
                        </tr>
                        <tr valign="top" id="apiDataModifierRow">
                            <th scope="row"><label for="apiDataModifier">JSON to add to requests:</label></th>
                            <td><textarea name="api-settings[apiDataModifier]" type="text" placeholder="JSON to add to requests" class="medium-text"><?php echo $options['apiDataModifier']; ?></textarea></td>
                        </tr>
                        <tr valign="top" id="berkeNoJobId">
                            <th scope="row"><label for="berkeNoJobId">The Berke Id that corresponds to "No Job":</label></th>
                            <td><input name="api-settings[berkeNoJobId]" type="text" placeholder="Berke No Job Id" class="medium-text" value="<?php echo $options['berkeNoJobId']; ?>" /></td>
                        </tr>
                    </table>
                    <?php
                        submit_button();
                    ?>
                </form>
            </div>
            <script type="text/javascript">
            jQuery(function() {
                jQuery('.settings-error').each(function(index, item) {
                    var id = item.id.substring(14);
                    var row = jQuery('#' + id + 'Row');
                    row.addClass('error');
                });
            });
            </script>

            <style>
            tr.error th label {
                color: red;
            }
            </style>
            <?php
        }

        function handleRequest($data) {
            echo 'request handled';
        }

        function apiIntegration_settings_link($links) {
            debugLog('Adding Settings link');
            $myLinks = array('<a href="options-general.php?page=apiIntegration_settings">Settings</a>');
            return array_merge($links, $myLinks);
        }
    }
}

global $ApiIntegrationPlugin;
$ApiIntegrationPlugin = new ApiIntegrationPlugin();


